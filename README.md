# TypeScript Boilerplate

This is a boilerplate for any node project using TypeScript.
Basic settings and template have been applied.

#

## Usage:

Development (Watch & Run):

```bash
npm run dev
```

Running:

```bash
npm run start
```

Building:

```bash
npm run build
```

#

Rie Takahashi - 2020.